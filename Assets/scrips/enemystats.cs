﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemystats : MonoBehaviour
{
    public float dmg = 10;
    GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("player");
    }

    // Update is called once per frame
    void Update()
    {
           
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.gameObject == player)
        {
            player.GetComponent<playerstats>().ReduceHealth(10);
        }
    }
}
