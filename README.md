# Revenge
Revenge is a new game I am creating :P

**This game is in a pre-alpha prototype state, and is not fun to play at the moment**

This project is being developed using Unity game engine.

2D platformer/fighter game vs AI where you fight a boss at the end of each stage.

Rough Story:

You are a regular person living life, except you have to fight "bosses" which are all either your own ailments (cancer, the cold, high blood pressure) (in boss form), and also people in your life who make things difficult (your boss, landlord, etc).

Each area is for example a specific part of your body or a specific place (like your workplace)

Preview of some basic mechanics in initial prototype:

https://www.youtube.com/watch?v=yCUOQq8Ex3g
